"use strict"

// Теоретичні питання 
// 1) Метод event.preventDefault() у JavaScript призначений для скасування типової поведінки браузера для конкретної події, 
// яка відбувається, наприклад, при кліку на посилання або відправці форми.

// 2) Прийом делегування подій полягає у використанні батьківського елемента для обробки подій дочірніх елементів.
// Це дозволяє зменшити кількість обробників подій на сторінці і покращити продуктивність, особливо при динамічних елементах.

// 3)Основні події документу та вікна браузера включають DOMContentLoaded (документ повністю завантажено), 
// load (всі ресурси завантажено), scroll (прокрутка сторінки), click (клік миші), keypress (натискання клавіші) і resize (зміна розміру вікна).

// Практичне Завдання

document.addEventListener('DOMContentLoaded', function () {
    const tabs = document.querySelectorAll('.tabs-title');
    const tabContent = document.querySelectorAll('.tabs-content li');

    document.querySelector('.tabs').addEventListener('click', (event) => {
        if (event.target.classList.contains('tabs-title')) {
            const clickedTab = event.target;
            const clickedTabIndex = Array.from(tabs).indexOf(clickedTab);

            tabs.forEach(tab => tab.classList.remove('active'));
        
            clickedTab.classList.add('active');

            tabContent.forEach(content => content.style.display = 'none');

            tabContent[clickedTabIndex].style.display = 'block';
        }
    });
});
